;;; More setup and configurations


;; The TODO files are not added to the agenda automatically.
(with-eval-after-load 'org-agenda
  (require 'org-projectile)
  (push (org-projectile:todo-files) org-agenda-files))

;; Ediff defaults
(defmacro csetq (variable value)
  `(funcall (or (get ',variable 'custom-set)
                'set-default)
            ',variable ,value))

(csetq ediff-window-setup-function 'ediff-setup-windows-plain)
(csetq ediff-split-window-function 'split-window-horizontally)
(csetq ediff-diff-options "-w")
(add-hook 'ediff-after-quit-hook-internal 'winner-undo)

;; Setup packages
(use-package dtrt-indent
  :config
  (dtrt-indent-mode 1)
  (setq dtrt-indent-verbosity 0) 
  :hook prog-mode-hook
  :diminish 'dtrt-indent-mode)


;; override certain bindings

(bind-keys*
 ("M-j" . find-file)
 ("M-J" . find-file-other-window)
 ("M-o" . ivy-switch-buffer)
 ("M-k" . kill-bufffer-and-window)
 ("M-K" . kill-buffer))

(bind-keys*
 ("C-o" . ace-window)
 ("C-;" . avy-goto-word-1)
 ("C-c ;" . avy-goto-char)
 ("M-g g" . avy-goto-line))

;; Use C-h for backspace as it is more ergonomic
(bind-keys*
 ("C-h" . backward-delete-char)
 ("M-h" . backward-kill-word))

;; Window movement
(bind-keys*
 ("C-x <up>" . windmove-up)
 ("C-x <down>" . windmove-down)
 ("C-x <left>" . windmove-left)
 ("C-x <right>" . windmove-right))

;; Kill/Copy full line if nothing is selected

(defun slick-cut (beg end)
  (interactive
   (if mark-active
       (list (region-beginning) (region-end))
     (list (line-beginning-position) (line-beginning-position 2)))))

(advice-add 'kill-region :before #'slick-cut)

(defun slick-copy (beg end)
  (interactive
   (if mark-active
       (list (region-beginning) (region-end))
     (message "Copied line")
     (list (line-beginning-position) (line-beginning-position 2)))))

(advice-add 'kill-ring-save :before #'slick-copy)


