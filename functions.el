;;; Collection of some utility functions

;; Proper key-mapping on a mac
(defun pm/mac-keys-setup()
  "Setup keys on mac keyboard to match regular layout."
  (interactive)
  (setq mac-command-modifier 'meta) ; make cmd key do Meta
  (setq mac-option-modifier 'super) ; make opt key do Super
  (setq mac-control-modifier 'control)) ; make Control key do Control
                                        ;(setq ns-function-modifier 'hyper)  ; make Fn key do Hyper


(defun pm/parent-directory (dir)
  "Get the parent directory of DIR."
  (unless (equal "/" dir)
    (file-name-directory (directory-file-name dir))))

(defun pm/find-file-in-heirarchy (current-dir fname)
  "Search for a file named FNAME upwards through the directory hierarchy, starting from CURRENT-DIR."
  (let ((file (concat current-dir fname))
        (parent (pm/parent-directory (expand-file-name current-dir))))
    (if (file-exists-p file)
        file
      (when parent
        (pm/find-file-in-heirarchy parent fname)))))

;; Setup GTAGSROOT when first called find-tags
(defun pm/set-gtags-root()
  "Find the GTAGSROOT directory by traversing upwards."
  (interactive)
  (let (root-path)
    (setq root-path (file-name-directory (pm/find-file-in-heirarchy (buffer-file-name) "GTAGS")))
    (if (string-blank-p root-path)
        ()
      (progn
        (message (concat "Setting GTAGSROOT to " root-path))
        (setenv "GTAGSROOT" root-path)))))

(defadvice find-tag (before my-set-gtags-root)
  "Find the GTAGSROOT if not already set."
  (progn
    (pm/set-gtags-root)))
(ad-activate 'find-tag)

(defadvice helm-gtags-dwim (before my-set-gtags-root2)
  (pm/set-gtags-root))
(ad-activate 'helm-gtags-dwim)

(defadvice counsel-gtags-dwim (before my-set-gtags-root3)
  (pm/set-gtags-root))
(ad-activate 'counsel-gtags-dwim)

;;(defadvice find-tag (after my-set-file-truename)
;;  (setq find-file-visit-truename 't))

(defun pm/split-buffer-on-regexp(regexp)
  (interactive "sEnter regexp:")
  (let (buf1 buf2 str1 str2)
    (save-excursion
      (goto-char (point-min))
      (search-forward-regexp regexp)
      (setq str1 (buffer-substring-no-properties (point-min) (point)))
      (setq str2 (buffer-substring-no-properties (point) (point-max))))
    (setq buf1 (get-buffer-create (concat (buffer-name) "-buf1")))
    (setq buf2 (get-buffer-create (concat (buffer-name) "-buf2")))
    (with-current-buffer buf1
      (erase-buffer)
      (insert str1))
    (with-current-buffer buf2
      (erase-buffer)
      (insert str2))
    (switch-to-buffer buf1)
    (split-window-sensibly)
    (other-window 1)
    (switch-to-buffer buf2)
    (goto-char (point-min))
    (other-window -1)))

(defun pm/text-between-regexp (regexp1 regexp2)
  "Extract out the text between two regexps in to a buffer."
  (interactive "sEnter regexp1:\nsEnter regexp2:")
  (let (buf start-point end-point text)
    (save-excursion
      (goto-char (point-min))
      (search-forward-regexp regexp1)
      (setq start-point (point))
      (search-forward-regexp regexp2)
      (setq end-point (point))
      (setq text (buffer-substring-no-properties start-point end-point)))
    (setq buf (get-buffer-create (concat (buffer-name) "-buf1")))
    (with-current-buffer buf
      (erase-buffer)
      (insert text))
    (split-window-sensibly)
    (other-window 1)
    (switch-to-buffer buf)
    (goto-char (point-min))))

(defun pm/get-client-from-config()
  "Find .p4config upwards"
  (interactive)
  (let (file ref-path)
    (setq file (pm/find-file-in-heirarchy (buffer-file-name) ".p4config"))
    (if (file-exists-p file)
        (progn 
;          (message "Found file %s" file)
          (find-file file)
          (goto-char 1)
          (search-forward "=")
          (setq ref-path (buffer-substring-no-properties (point) (line-end-position)))
          (message "Your ref-path is %s" ref-path)
          (kill-buffer (current-buffer))
          ref-path) ; return ref-path name
      nil)))

(defun pm/set-client()
  "Set P4CLIENT environment variable"
  (interactive)
  (let (ref-path)
    (progn
      (setq ref-path (pm/get-client-from-config))
      (setenv "P4CLIENT" ref-path))))

(require 'p4)
(defadvice p4-call-command (before my-set-p4-client())
  (pm/set-client))
(ad-activate 'p4-call-command)

(defvar pm/dispatch-cmd-name "qsub"
  "Use this command to dispach to grid machines")

(defun pm/dispatch-scr-on-grid(scr)
  "Fire the script on the grid"
  (interactive "fEnter the script")
  (let (cmd-name)
    (setq cmd-name (format "%s %s" pm/dispatch-cmd-name scr))
    (shell-command cmd-name)))

(defvar pm/git-repo-dir "~/.emacs.d/fromgit/"
  "Location where Emacs packages through git are installed.")

(defun pm/get-git-repo (url name)
  "Get a git repo from URL and save it at NAME."
  (interactive "sEnter URL: \nsEnter name: ")
  (let* ((full-name (concat pm/git-repo-dir  name))
         (cmd (concat "git clone " url " " full-name)))
    (unless (file-exists-p full-name)
      (shell-command cmd))
    (add-to-list 'load-path full-name)))


(defun pm/update-git-repo ()
  "Update the installed git repo packages"
  (interactive)
  (let* ((file-list (directory-files pm/git-repo-dir t "^\\([^.]\\|\\.[^.]\\|\\.\\..\\)")))
    (dolist (f file-list)
      (when (file-exists-p (concat f "/.git"))
        (let ((cmd (concat "cd " f "; git pull origin master ; cd -")))
          (message (concat "Updating " f))
          (shell-command cmd))))))

(defcustom pm/ediff-search-done nil
  "Save if ediff-search is done.")

(defcustom pm/ediff-search-buf 1
  "Save which buffer was searched.")

;; For call-interactively part, the only way
;; to go back is to have a hook because it
;; does not wait for isearch to complete.
(defun pm/ediff-search (buf repeat)
  (setq pm/ediff-search-done t)
  (setq pm/ediff-search-buf buf)
  (other-window buf)
  (if repeat
      (progn
        (search-forward-regexp isearch-string)
        (pm/ediff-search-back))
    (call-interactively 'isearch-forward-regexp)))

(defun pm/ediff-jump-to-diff (buf-no)
  (if (eq buf-no 2)
      (ediff-jump-to-difference (ediff-diff-at-point 'B))
    (ediff-jump-to-difference (ediff-diff-at-point 'A))))

(defun pm/ediff-search-back()
  (if pm/ediff-search-done
      (progn
        (other-window (- 3 pm/ediff-search-buf))
        (pm/ediff-jump-to-diff pm/ediff-search-buf)))
  (setq pm/ediff-search-done nil))

(add-hook 'isearch-mode-end-hook 'pm/ediff-search-back)

(defun pm/ediff-search-A ()
  (interactive)
  (pm/ediff-search 1 nil))

(defun pm/ediff-search-B ()
  (interactive)
  (pm/ediff-search 2 nil))

(defun pm/ediff-search-repeat()
  (interactive)
  (pm/ediff-search pm/ediff-search-buf t))

(defun pm/ediff-setup-keys()
  (ediff-setup-keymap)
  (define-key ediff-mode-map "sA" 'pm/ediff-search-A)
  (define-key ediff-mode-map "sB" 'pm/ediff-search-B)
  (define-key ediff-mode-map "sa" 'pm/ediff-search-A)
  (define-key ediff-mode-map "sb" 'pm/ediff-search-B)
  (define-key ediff-mode-map "ss" 'pm/ediff-search-repeat))

(add-hook 'ediff-mode-hook 'pm/ediff-setup-keys)

(provide 'functions)
